package com.example.volskyioleh.psntest.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.volskyioleh.psntest.GenerateActivity;
import com.example.volskyioleh.psntest.R;

public class ChooseCouponFragment extends Fragment implements View.OnClickListener {


    public ChooseCouponFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_choose_coupon, container, false);

        ImageView disc50 = view.findViewById(R.id.discc5050);
        ImageView disc20 = view.findViewById(R.id.disc20);
        ImageView disc10 = view.findViewById(R.id.disc10);
        ImageView card200 = view.findViewById(R.id.card200);
        ImageView card100 = view.findViewById(R.id.card100);
        ImageView card50 = view.findViewById(R.id.card50);
        ImageView card25 = view.findViewById(R.id.card25);
        ImageView card10 = view.findViewById(R.id.card10);

        disc50.setOnClickListener(this);
        disc20.setOnClickListener(this);
        disc10.setOnClickListener(this);
        card200.setOnClickListener(this);
        card100.setOnClickListener(this);
        card50.setOnClickListener(this);
        card25.setOnClickListener(this);
        card10.setOnClickListener(this);



        return view;
    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getContext(), GenerateActivity.class);
        switch (v.getId()) {

            case R.id.discc5050:
                Log.d("fffff","allliiiwee11");
                break;
            case R.id.disc20:
                Log.d("fffff","allliiiwee22");
                break;
            case R.id.disc10:

                break;
            case R.id.card200:

                break;
            case R.id.card100:

                break;
            case R.id.card50:

                break;
            case R.id.card25:

                break;
            case R.id.card10:

                break;
        }
        startActivity(intent);
    }
}
