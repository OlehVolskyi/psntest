package com.example.volskyioleh.psntest;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.adcolony.sdk.AdColony;
import com.appnext.base.Appnext;
import com.fyber.Fyber;
import com.vungle.publisher.VunglePub;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity {
    final VunglePub vunglePub = VunglePub.getInstance();


    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Appnext.init(MainActivity.this);
        final String app_id = "59ad0123c4421b8c640013e2";
        Button getStartBtn = findViewById(R.id.get_start_Btn);
        vunglePub.init(this, app_id);
        getStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TabActivity.class);
                startActivity(intent);
            }
        });

        AdColony.configure(this, "app68ab01ff3f554d6494", "vzfdf03d56aaea405e95");

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        vunglePub.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        vunglePub.onResume();
        Fyber.with("108661", this).withSecurityToken("ac7fdb580f05cc75599c6bf8ee3a30c4").start();
    }

}
