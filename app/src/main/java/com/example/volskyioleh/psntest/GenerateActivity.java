package com.example.volskyioleh.psntest;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.appnext.banners.BannerAdRequest;
import com.appnext.banners.BannerView;
import com.appnext.base.Appnext;


public class GenerateActivity extends AppCompatActivity {

    private int mPrice;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate);
        final EditText editText = findViewById(R.id.editText);
        Toolbar actionBar = findViewById(R.id.toolbar_top);
        setSupportActionBar(actionBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final TextView coinsTv = findViewById(R.id.coins);
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        coinsTv.setText(String.valueOf(sharedPreferences.getInt("COINS", 0)));

        Appnext.init(GenerateActivity.this);
        BannerView bannerView = findViewById(R.id.banner);
        bannerView.loadAd(new BannerAdRequest());

        ImageView imageView = findViewById(R.id.back_btn);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageView redeem = findViewById(R.id.redembtn);
        redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isEmptyEmail(editText.getText().toString())) {
                    dialog("No email address", "Email address can’t be blank");
                } else if (!isValidEmail(editText.getText().toString())) {
                    dialog("Invalid Email", "Your Email address in invalid");
                }

                if (isValidEmail(editText.getText().toString())) {
                    dialog("Error", "Generate code at first, please");
                }
            }
        });

        ImageView generate = findViewById(R.id.generatebtn);
        generate.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                mPrice = 28000;
                int currentCoins = sharedPreferences.getInt("COINS", 0);
                if (mPrice > currentCoins) {
                    dialog("Sorry", "You don’t have enough coins");
                } else if (!isEmptyEmail(editText.getText().toString())) {
                    dialog("No email address", "Email address can’t be blank");
                } else if (!isValidEmail(editText.getText().toString())) {
                    dialog("Invalid Email", "Your Email address in invalid");
                } else {
                    SharedPreferences.Editor ed = sharedPreferences.edit();
                    ed.putInt("COINS", currentCoins - mPrice);
                    ed.apply();
                    coinsTv.setText(String.valueOf(sharedPreferences.getInt("COINS", 0)));
                    dialog("Excellent", "You will get your code in 72 hours");
                }
            }
        });


    }

    public void dialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean isEmptyEmail(String target) {
        return !TextUtils.isEmpty(target);
    }


    public boolean isValidEmail(String target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

}
