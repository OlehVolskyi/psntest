package com.example.volskyioleh.psntest.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.example.volskyioleh.psntest.R;
import com.example.volskyioleh.psntest.TabActivity;
import com.fyber.ads.AdFormat;
import com.fyber.currency.VirtualCurrencyErrorResponse;
import com.fyber.currency.VirtualCurrencyResponse;
import com.fyber.requesters.OfferWallRequester;
import com.fyber.requesters.RequestCallback;
import com.fyber.requesters.RequestError;
import com.fyber.requesters.VirtualCurrencyCallback;
import com.fyber.requesters.VirtualCurrencyRequester;
import com.vungle.publisher.EventListener;
import com.vungle.publisher.VunglePub;

import static android.content.ContentValues.TAG;


public class EarnCoinsFragment extends Fragment implements View.OnClickListener {
    private final VunglePub vunglePub = VunglePub.getInstance();
    private AdColonyInterstitial mAdColonyInterstitial;
    public Intent mOfferwallIntent;

    VirtualCurrencyCallback virtualCurrencyCallback = new VirtualCurrencyCallback() {
        @Override
        public void onSuccess(VirtualCurrencyResponse virtualCurrencyResponse) {
            // Reward your user based on the deltaOfCoins parameter
            double deltaOfCoins = virtualCurrencyResponse.getDeltaOfCoins();
            ((TabActivity) getActivity()).setCoins((int) deltaOfCoins);

        }

        @Override
        public void onRequestError(RequestError requestError) {
            // No reward has been returned, so nothing can be provided to the user
            Log.d(TAG, "request error: " + requestError.getDescription());
        }

        @Override
        public void onError(VirtualCurrencyErrorResponse virtualCurrencyErrorResponse) {
            // No reward has been returned, so nothing can be provided to the user
            Log.d(TAG, "VCS error received - " + virtualCurrencyErrorResponse.getErrorMessage());
        }
    };

    RequestCallback requestCallback = new RequestCallback() {
        @Override
        public void onAdAvailable(Intent intent) {
            // Store the intent that will be used later to show the Offer Wall
            mOfferwallIntent = intent;
            Log.d("FYBER", "Offers are available");
        }

        @Override
        public void onAdNotAvailable(AdFormat adFormat) {
            // Since we don't have an ad, it's best to reset the Offer Wall intent
            mOfferwallIntent = null;
            Log.d(TAG, "No ad available");
        }

        @Override
        public void onRequestError(RequestError requestError) {
            // Since we don't have an ad, it's best to reset the Offer Wall intent
            mOfferwallIntent = null;
            Log.d(TAG, "Something went wrong with the request: " + requestError.getDescription());
        }
    };

    AdColonyInterstitialListener listener = new AdColonyInterstitialListener() {
        @Override
        public void onClosed(AdColonyInterstitial ad) {
            super.onClosed(ad);
            ((TabActivity) getActivity()).setCoins(10);
        }

        @Override
        public void onRequestFilled(AdColonyInterstitial ad) {
            /* Store and use this ad object to show your ad when appropriate */
            mAdColonyInterstitial = ad;
        }
    };

    public EarnCoinsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private void onLevelStart() {
        vunglePub.playAd();
    }


    @Override
    public void onPause() {
        super.onPause();
        vunglePub.onPause();
        AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
    }

    @Override
    public void onResume() {
        super.onResume();
        vunglePub.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_earn_coins, container, false);

        AdColony.configure(getActivity(), "app68ab01ff3f554d6494", "vzfdf03d56aaea405e95");


        vunglePub.setEventListeners(vungleDefaultListener);

        ImageView quickOffers = view.findViewById(R.id.quick_offers);
        ImageView proTasks = view.findViewById(R.id.pro_tasks);
        ImageView dailyOffers = view.findViewById(R.id.daily_offers);
        ImageView watchVideo = view.findViewById(R.id.watch_video);

        AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);

        OfferWallRequester.create(requestCallback).request(getActivity());
        VirtualCurrencyRequester.create(virtualCurrencyCallback)
                .request(getActivity());

        quickOffers.setOnClickListener(this);
        proTasks.setOnClickListener(this);
        dailyOffers.setOnClickListener(this);
        watchVideo.setOnClickListener(this);

        return view;
    }


    @Override
    public void onDestroy() {
        vunglePub.clearEventListeners();
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.quick_offers:
                startActivity(mOfferwallIntent);
                break;
            case R.id.daily_offers:
                onLevelStart();
                break;
            case R.id.watch_video:
                if (mAdColonyInterstitial != null) {
                    mAdColonyInterstitial.show();
                    AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
                    if(mAdColonyInterstitial.getListener()!=null){
                    mAdColonyInterstitial.getListener().onExpiring(mAdColonyInterstitial);}
                } else {
                    AdColony.requestInterstitial("vzfdf03d56aaea405e95", listener);
                    Toast.makeText(getActivity(), "Task is not ready", Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    private final EventListener vungleDefaultListener = new EventListener() {
        @Override
        public void onAdStart() {
            // Called before playing an ad.
        }

        @Override
        public void onAdUnavailable(String reason) {
            // Called when VunglePub.playAd() was called but no ad is available to show to the user.
        }

        @Override
        public void onAdPlayableChanged(boolean b) {
            Log.d("DefaultListener", "This is a default eventlistener.");
        }

        @Override
        public void onAdEnd(boolean wasSuccessfulView, boolean wasCallToActionClicked) {
            if (wasSuccessfulView) {
                ((TabActivity) getActivity()).setCoins(10);
            }
            // Called when the user leaves the ad and control is returned to your application.
        }
    };
}
