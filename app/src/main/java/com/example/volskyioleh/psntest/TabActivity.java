package com.example.volskyioleh.psntest;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.appnext.banners.BannerAdRequest;
import com.appnext.banners.BannerView;
import com.appnext.base.Appnext;
import com.example.volskyioleh.psntest.Fragments.ChooseCouponFragment;
import com.example.volskyioleh.psntest.Fragments.EarnCoinsFragment;
import com.example.volskyioleh.psntest.Fragments.InstructionsFragment;
import com.vungle.publisher.VunglePub;

public class TabActivity extends AppCompatActivity {
    final VunglePub vunglePub = VunglePub.getInstance();
    private TextView mCoinsTv;
    private SharedPreferences coinsSP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        Toolbar actionBar = findViewById(R.id.toolbar_top2);
        setSupportActionBar(actionBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Appnext.init(TabActivity.this);
        ViewPager mPager = findViewById(R.id.viewpager);
        setupViewPager(mPager);
        mCoinsTv = findViewById(R.id.coins);

        BannerView bannerView = findViewById(R.id.banner);
        bannerView.loadAd(new BannerAdRequest());

        TabLayout mTabLayout = findViewById(R.id.tablayout);
        mTabLayout.setupWithViewPager(mPager);
        coinsSP = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mCoinsTv.setText(String.valueOf(coinsSP.getInt("COINS", 0)));

    }


    public void setCoins(int coins_data) {
        int current_coins = coinsSP.getInt("COINS", 0);
        SharedPreferences.Editor ed = coinsSP.edit();
        ed.putInt("COINS", current_coins + coins_data);
        ed.apply();
        mCoinsTv.setText(String.valueOf(coinsSP.getInt("COINS", 0)));
    }

    @Override
    protected void onPause() {
        super.onPause();
        vunglePub.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        vunglePub.onResume();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ChooseCouponFragment(), "Choose Coupon");
        adapter.addFragment(new EarnCoinsFragment(), "Earn Coins");
        adapter.addFragment(new InstructionsFragment(), "Instructions");
        viewPager.setAdapter(adapter);
    }
}
